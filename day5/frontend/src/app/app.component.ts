import {Component} from '@angular/core';
import {AuthService} from "./feature/login/auth.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'Digitall Angular Workshop - Day 3';

    constructor(private authService: AuthService,private router: Router) {

    }

    isLoggedIn() {
        return this.authService.isLoggedIn();
    }

    logout() {

        this.authService.logout();
        this.router.navigate(['/login']);
    }
}
