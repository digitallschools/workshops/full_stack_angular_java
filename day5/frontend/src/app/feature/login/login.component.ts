import {
    Component,
    OnInit
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import {
    AuthService
} from './auth.service'
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    submitted = false;

    constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
    }

    ngOnInit() {
        if(this.authService.isLoggedIn()){
            this.router.navigate(['/projects']);
            return;
        }

        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required]],
            password: ['', [Validators.required, Validators.minLength(6)]],
        });
    }


    get controls() {
        return this.loginForm.controls;
    }

    onSubmit() {
        this.submitted = true;

        if (this.loginForm.invalid) {
            return;
        }
        this.authService.login(this.loginForm.value)
            .subscribe(jwtResponse => {
                this.router.navigate(['/projects'])
            }, () => {
                alert("failed to login");
            })
    }
}