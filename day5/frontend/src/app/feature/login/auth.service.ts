import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from "../../../environments/environment";
import {JwtResponse} from "../../model/jwt-response";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class AuthService {


    constructor(private http: HttpClient, private router: Router) {
    }

    login(creds): Observable<JwtResponse> {
        const post_url = `${environment.API_URL}/authenticate`;
        return this.http.post<JwtResponse>(post_url, creds)
            .pipe(
                map(jwtResponse => {
                    localStorage.setItem('token', jwtResponse.token);
                    return jwtResponse;
                })
            );
    }

    isLoggedIn(): boolean {
        return !!localStorage.getItem("token");
    }

    logout() {
        localStorage.removeItem("token");
    }
}
