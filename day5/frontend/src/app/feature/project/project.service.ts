import { Injectable } from '@angular/core';
import { Project } from '../../model/project';
import { ProjectType } from '../../model/project.type';
import { ProjectStatus } from '../../model/project.status';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, filter, take, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { JwtResponse } from '../../model/jwt-response';
import { Router } from '@angular/router';

@Injectable()
export class ProjectService {
  private projects: Project[] = [];
  private projectListSubject = new BehaviorSubject<Project[]>([]);
  public projectList$: Observable<
    Project[]
  > = this.projectListSubject.asObservable();

  selectedProject: Project;
  private selectedProjectSubject = new BehaviorSubject<Project>(null);
  public selectedProject$ = this.selectedProjectSubject.asObservable();

  public totalProjects$  = this.projectList$.pipe(map((projects)=> projects.length));


    public scrumProjectCount$  = this.projectList$.pipe(map((projects)=> {
        const scrumProjects = projects.filter((project) => {
            return project.type == ProjectType.SCRUM;
        });
        return scrumProjects.length;
    }));

    public kanbanProjectCount$  = this.projectList$.pipe(map((projects)=> {
        const scrumProjects = projects.filter((project) => {
            return project.type == ProjectType.KANBAN;
        });
        return scrumProjects.length;
    }));


  constructor(private http: HttpClient, private router: Router) {}


  loadProjects(): void {
    const project_list_url = `${environment.API_URL}/api/projects`;
    this.http.get<Project[]>(project_list_url).subscribe(projectList => {
      this.projectListSubject.next( this.projects = projectList);
      this.selectedProject = this.projects[0];
      this.selectedProjectSubject.next(this.selectedProject);
      return projectList;
    });
  }

  addProject(project: Project){
    const project_create_url = `${environment.API_URL}/api/projects`;
    this.http.post<Project>(project_create_url, project).subscribe(project => {
      this.projects.unshift(project);
      this.projectListSubject.next(this.projects);
      this.selectedProjectSubject.next(project);
      this.router.navigate(['/projects']);
    });
  }

  addProject2(project: Project){
    const project_create_url = `${environment.API_URL}/api/projects`;
    this.http
      .post<Project>(project_create_url, project)
      .subscribe(() => this.loadProjects());
  }

  setSelectedProject(project) {
    this.selectedProjectSubject.next(this.selectedProject = project);
  }

  setProjectsBySearchTerm(searchText) {
    const updatedProjects =  this.projects.filter((project) => {
        return project.name.includes(searchText);
    });

    this.projectListSubject.next(updatedProjects);
}


  loadProjectsByStatus(status) {
    if (status == 'all') {
      this.projectListSubject.next(this.projects);
    } else {
      const filteredProjects = this.projects.filter(project => {
        return project.status == status;
      });
      this.projectListSubject.next(filteredProjects);
    }
  }

  loadProjectsByType(type) {
    const filteredProjects = this.projects.filter(project => {
      return project.type == type;
    });
    this.projectListSubject.next(filteredProjects);
  }
}
