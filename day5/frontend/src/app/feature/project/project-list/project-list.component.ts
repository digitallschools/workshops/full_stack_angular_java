import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import {ProjectService} from '../project.service';
import {Project} from "../../../model/project";
import {ProjectType} from "../../../model/project.type";
import {Observable} from 'rxjs';

@Component({
    selector: 'app-project-list',
    templateUrl: './project-list.component.html',
    styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit{

    projects:Project[];

    @Input() selectedProject;

    @Output() projectSelected = new EventEmitter<Project>();

    constructor(private projectService: ProjectService) { };
    ngOnInit() {
        this.projectService.projectList$.subscribe((projects) => {
            this.projects = projects;
        });

        this.projectService.loadProjects();
    }

    onProjectClick(project) {
        this.projectService.setSelectedProject(project);
    }

    getActiveProjectClass(project) {
        return project.id == this.selectedProject.id;
    }

    onSearchProject(searchTerm) {
       this.projectService.setProjectsBySearchTerm(searchTerm);
    }

    onFilterByProjectStatus(status) {
       this.projectService.loadProjectsByStatus(status);
    }

    onFilterByProjectType(type) {
         this.projectService.loadProjectsByType(type);
    }

}
