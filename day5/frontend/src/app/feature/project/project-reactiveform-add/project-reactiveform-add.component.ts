import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProjectType} from '../../../model/project.type';
import {UserService} from '../../user/user.service';
import {User} from '../../../model/user';
import {ProjectService} from "../project.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-project-reactiveform-add',
    templateUrl: './project-reactiveform-add.component.html',
    styleUrls: ['./project-reactiveform-add.component.css']
})
export class ProjectReactiveformAddComponent implements OnInit {
    projectCreationForm: FormGroup;
    submitted = false;
    projectTypeOptions;
    userOptions: User[];

    constructor(
        private formBuilder: FormBuilder,
        private userService: UserService,
        private projectService: ProjectService,
        private router: Router
    ) {
        this.projectTypeOptions = ProjectType;
        this.userOptions = this.userService.getUserList();
    }

    ngOnInit() {
        this.projectCreationForm = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(6)]],
            description: ['', [Validators.required]],
            type: [null, [Validators.required]],
            owner: [null, [Validators.required]]
        });
    }

    // convenience getter for easy access to form fields
    get controls() {
        return this.projectCreationForm.controls;
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.projectCreationForm.invalid) {
            return;
        }

        this.projectService.addProject(this.projectCreationForm.value);
    }
}
