import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ProjectDashboardComponent } from './project-dashboard/project-dashboard.component';
import { ProjectReactiveformAddComponent } from './project-reactiveform-add/project-reactiveform-add.component';
import { ProjectTemplateformAddComponent } from './project-templateform-add/project-templateform-add.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectTypeComponent } from './project-type/project-type.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ProjectSummaryComponent } from './project-summary/project-summary.component';
import { ProjectFilterComponent } from './project-filter/project-filter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const projectRoutes: Routes = [
  { path: '', component: ProjectDashboardComponent },
  {
    path: 'reactive-form-add',
    component: ProjectReactiveformAddComponent
  },
  {
    path: 'template-form-add',
    component: ProjectTemplateformAddComponent
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(projectRoutes),
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    ProjectDashboardComponent,
    ProjectListComponent,
    ProjectTypeComponent,
    ProjectSummaryComponent,
    ProjectDetailsComponent,
    ProjectFilterComponent,
    ProjectReactiveformAddComponent,
    ProjectTemplateformAddComponent
  ]
})
export class ProjectModule {}
