import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { UserGenderComponent } from './user-gender/user-gender.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const userRoutes: Routes = [{ path: '', component: UserListComponent }];

@NgModule({
  imports: [RouterModule.forChild(userRoutes), CommonModule,    ReactiveFormsModule,
    FormsModule],
  declarations: [UserListComponent, UserGenderComponent]
})
export class UserModule {}
