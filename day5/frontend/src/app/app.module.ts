import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutesModule } from './app-routes.module';

import { RouterModule } from '@angular/router';

import { ProjectService } from './feature/project/project.service';

import { UserService } from './feature/user/user.service';
import { AuthService } from './feature/login/auth.service';
import { JwtInterceptor } from './feature/login/JwtInterceptor';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AppRoutesModule,
    RouterModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    ProjectService,
    UserService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
