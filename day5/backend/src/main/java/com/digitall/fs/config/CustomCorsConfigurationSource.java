package com.digitall.fs.config;

import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mathdisk on 1/21/2017.
 */
public class CustomCorsConfigurationSource implements CorsConfigurationSource {


    @Override
    public CorsConfiguration getCorsConfiguration(HttpServletRequest httpServletRequest) {
        List<String> allowedOrigins = new ArrayList<>();
        allowedOrigins.add(httpServletRequest.getHeader("Origin"));
        allowedOrigins.add("*");

        List<String> allowedMethods = new ArrayList<>();
        allowedMethods.add("GET");
        allowedMethods.add("POST");
        allowedMethods.add("DELETE");
        allowedMethods.add("OPTIONS");

        CorsConfiguration corsConfiguration =  new CorsConfiguration();
        corsConfiguration.addAllowedHeader("Content-Type, Accept, X-Authorization");
        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.setAllowedOrigins(allowedOrigins);
        corsConfiguration.setAllowedMethods(allowedMethods);
        corsConfiguration.setMaxAge(3600L);
        return corsConfiguration;
    }
}
