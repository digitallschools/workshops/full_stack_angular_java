package com.digitall.fs.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class Project {

    String id;
    String name;
    String description;
    String owner;
    ProjectType type;
    String startDate;
    ProjectStatus status;

    public Project(){

    }
    public Project(String id, String name, String description, String owner, ProjectType type, String startDate, ProjectStatus status) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.type = type;
        this.startDate = startDate;
        this.status = status;
    }
}
