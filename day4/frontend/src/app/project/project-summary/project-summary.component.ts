import {Component, OnInit} from '@angular/core';
import {ProjectService} from "../project.service";
import {ProjectType} from "../../model/project.type";

@Component({
    selector: 'app-project-summary',
    templateUrl: './project-summary.component.html',
    styleUrls: ['./project-summary.component.css']
})
export class ProjectSummaryComponent implements OnInit {

    totalProjectCount;
    scrumProjectCount;
    kanbanProjectCount;

    constructor(private projectService: ProjectService) {
    }

    ngOnInit() {
        this.projectService.totalProjects$.subscribe((totalProjectCount) => {
            this.totalProjectCount = totalProjectCount;
          });
          this.projectService.scrumProjectCount$.subscribe((scrumProjectCount) => {
            this.scrumProjectCount = scrumProjectCount;
          });
          this.projectService.kanbanProjectCount$.subscribe((kanbanProjectCount) => {
            this.kanbanProjectCount = kanbanProjectCount;
          });    
    }

}
