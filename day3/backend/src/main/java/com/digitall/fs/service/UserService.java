package com.digitall.fs.service;

import com.digitall.fs.entity.User;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;


@Service
public class UserService {


    private static List<User> users = new ArrayList<>();


    @PostConstruct
    public void init() {
        users = Arrays.asList(
                new User("1", "some@gmail.com"),
                new User("1", "some@gmail.com")
        );
    }


    public HashMap<String,User> getAllUsers() {
        HashMap usermap =  new HashMap<String,User>();
        usermap.put(users.get(0).getId(),users.get(0));
        return  usermap;
    }

    public Optional<User> getUserById(String id) {
        return Optional.empty();
    }

    public User createUser(User user) {
        return user;
    }

    public User updateUser(User user) {
        return user;
    }

    public void deleteUser(String userId) {

    }


    public Long countUsersByDomain(String emailDomain) {

        List<User> allUsers = new ArrayList<>();

        return allUsers.stream().filter((user) -> user.getEmail().endsWith(emailDomain)).count();
    }
}
