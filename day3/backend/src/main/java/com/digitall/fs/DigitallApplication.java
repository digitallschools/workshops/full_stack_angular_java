package com.digitall.fs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitallApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigitallApplication.class, args);
    }
}
